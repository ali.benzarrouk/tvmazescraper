﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TvMazeScraper.InterfaceContracts;
using static TvMazeScraper.Models.TvMazeContext;

namespace TvMazeScraper.Services
{
    public class DatabaseSeedInitializer : IDatabaseSeedInitializer
    {
        private IInterfaceContractEntityTransformer contractEntityTransformer;

        static HttpClient client = new HttpClient();
        static readonly string castsPath = "http://api.tvmaze.com/shows/{0}/cast";
        static readonly string showsPath = "http://api.tvmaze.com/shows?page={0}";

        public DatabaseSeedInitializer(IInterfaceContractEntityTransformer contractEntityTransformer)
        {
            this.contractEntityTransformer = contractEntityTransformer;
        }

        private Task<List<CastPerson>> GetInitialCastsAsync(int showID)
        {
            HttpResponseMessage response = client.GetAsync(String.Format(castsPath, showID)).Result;
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<List<CastPerson>>();
            }

            return Task.FromResult<List<CastPerson>>(null);
        }

        private Task<List<Show>> GetInitialSummarizedShowsAsync(int page)
        {
            HttpResponseMessage response = client.GetAsync(String.Format(showsPath, page)).Result;
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<List<Show>>();
            }

            return Task.FromResult<List<Show>>(null);
        }

        public IAsyncEnumerable<SummarizedShowEntity> GetInitialSummarizedShowsAsync()
        {
            int page = 1;
            Task<List<Show>> taskShows = GetInitialSummarizedShowsAsync(page);
            List<Task<List<CastPerson>>> castsTask = new List<Task<List<CastPerson>>>();

            List<Show> shows = new List<Show>();
            List<CastPerson> casts = new List<CastPerson>();

            while (taskShows.Result != null && taskShows.Result.Count > 0)
            {
                page = page + 1;
                List<Show> currentTaskShows = taskShows.Result;
                taskShows = GetInitialSummarizedShowsAsync(page);
                shows.AddRange(currentTaskShows);

                foreach (var show in currentTaskShows)
                {
                    yield return contractEntityTransformer.From(show, GetInitialCastsAsync(show.id));
                }
            }
        }
    }
}
