﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.InterfaceContracts;
using static TvMazeScraper.Models.TvMazeContext;

namespace TvMazeScraper.Services
{
    public class InterfaceContractEntityTransformer : IInterfaceContractEntityTransformer
    {
        private IInterfaceContractEntityTransformer contractEntityTransformer;

        public InterfaceContractEntityTransformer(IInterfaceContractEntityTransformer contractEntityTransformer)
        {
            this.contractEntityTransformer = contractEntityTransformer;
        }

        public SummarizedShowEntity From(Show show, Task<List<CastPerson>> castPersonsTask)
        {
            if (show == null)
            {
                return null;
            }

            List<CastPerson> castPersons = castPersonsTask.Result;

            return new SummarizedShowEntity
            {
                id = show.id,
                name = show.name,
                cast = castPersons?.Select((CastPerson arg) =>
                {
                    return new CastEntity()
                    {
                        id = arg.character.id,
                        name = arg.character.name,
                        birthday = arg.person.birthday
                    };
                }).ToList()
            };
        }
    }
}
