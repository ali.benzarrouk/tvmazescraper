﻿using System.Collections.Generic;
using static TvMazeScraper.Models.TvMazeContext;

namespace TvMazeScraper.Services
{
    public interface IDatabaseSeedInitializer
    {
        IAsyncEnumerable<SummarizedShowEntity> GetInitialSummarizedShowsAsync();
    }
}
