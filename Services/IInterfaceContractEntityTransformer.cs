﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TvMazeScraper.InterfaceContracts;
using static TvMazeScraper.Models.TvMazeContext;

namespace TvMazeScraper.Services
{
    public interface IInterfaceContractEntityTransformer
    {
        SummarizedShowEntity From(Show show, Task<List<CastPerson>> castPersonsTask);
    }
}
