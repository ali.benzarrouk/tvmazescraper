Hi all,

This application runs dockerized. It will install a dockerized version of sql server and use it.
Please start launch.sh script. You will need passwords to install sql server.

On start up it will populate DB.

It will be deployed on localhost:8080, you can then get the list of summarized shows, paginated through the url localhost:8080/api/summarizedshow?page=1

Page size is 10.
