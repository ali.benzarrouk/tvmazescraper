﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using TvMazeScraper.Services;

namespace TvMazeScraper.Models
{
    public class TvMazeContext : DbContext
    {
        private IDatabaseSeedInitializer databaseSeedInitializer;

        public TvMazeContext(DbContextOptions<TvMazeContext> options, IDatabaseSeedInitializer databaseSeedInitializer)
            : base(options)
        {
            this.databaseSeedInitializer = databaseSeedInitializer;
        }

        public DbSet<CastEntity> Casts { get; set; }
        public DbSet<SummarizedShowEntity> SummarizedShows { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region summarized show seed
            IAsyncEnumerator<SummarizedShowEntity> enumm = databaseSeedInitializer.GetInitialSummarizedShowsAsync().GetEnumerator();

            while (enumm.MoveNext().Result)
            {
                modelBuilder.Entity<SummarizedShowEntity>().HasData(enumm.Current);
            }
            #endregion
        }

        public class CastEntity
        {
            public int id { get; set; }
            public string name { get; set; }
            public string birthday { get; set; }
        }

        public class SummarizedShowEntity
        {
            public int id { get; set; }
            public string name { get; set; }

            public ICollection<CastEntity> cast { get; set; }
        }
    }
}