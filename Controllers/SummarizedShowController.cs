using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TvMazeScraper.Models;
using static TvMazeScraper.Models.TvMazeContext;

namespace TvMazeScraper.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SummarizedShowController : ControllerBase
    {
        private readonly TvMazeContext _context;
        private readonly int PAGE_SIZE = 10;

        public SummarizedShowController(TvMazeContext context)
        {
            _context = context;
        }

        // GET: api/SummarizedShow
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<SummarizedShowEntity>>> GetSummarizedShows([FromQuery(Name = "page")] int page)
        {
            page = page == 0 ? 1 : page;
            return Ok(await _context.SummarizedShows.Skip(PAGE_SIZE * (page - 1)).Take(PAGE_SIZE).ToListAsync());
        }
    }
}
